import React, { useState, useEffect } from 'react';
import axios from 'axios';

class TableTitle extends React.Component {
  render() {
    return <h1 className="center">Table Harga Buah</h1>;
  }
}

class TableHead extends React.Component {
  render() {
    return (
      <thead>
        <tr>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
        </tr>
      </thead>
    );
  }
}

const FruitLists14 = () => {
  const [daftarHargaBuah, setDaftarHargaBuah] =  useState([])
  const [inputNamaBuah, setInputNamaBuah]  =  useState("")
  const [inputHargaBuah, setInputHargaBuah]  =  useState(0)
  const [inputBeratBuah, setInputBeratBuah]  =  useState(0)
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [idOfList, setIdOfList] =  useState(-1)

  useEffect(() => {
    if (daftarHargaBuah.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setDaftarHargaBuah(newData);
        })
    }
  })


  const handleDelete = (event) => {
    let index = event.target.value
    let id = event.target.id;
    let newDaftarHargaBuah = daftarHargaBuah
    newDaftarHargaBuah.splice(index, 1)

    setDaftarHargaBuah([...newDaftarHargaBuah])

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
  }

  const handleEdit = (event) =>{
    let index = event.target.value;
    let id = event.target.id;
    let dataBuah = daftarHargaBuah[index];


    setInputNamaBuah(dataBuah.name)
    setInputHargaBuah(dataBuah.price)
    setInputBeratBuah(dataBuah.weight)
    setIndexOfForm(index)
    setIdOfList(id);
  }

  const handleChange = (event) =>{
    const targetName = event.target.name;
    const targetValue = event.target.value;

    if (targetName === "inputNamaBuah") {
      setInputNamaBuah(targetValue);
    } else if (targetName === "inputHargaBuah") {
      setInputHargaBuah(targetValue);
    } else if (targetName === "inputBeratBuah") {
      setInputBeratBuah(targetValue);
    }
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let name = inputNamaBuah
    let price = +inputHargaBuah
    let weight = +inputBeratBuah


    if (name.replace(/\s/g,'') !== ""){
      let newDaftarHargaBuah = daftarHargaBuah
      let index = indexOfForm;
      let id = idOfList;

      console.log(indexOfForm, id);

      if (index === -1){
        newDaftarHargaBuah = [...newDaftarHargaBuah, {name, price, weight}]
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
      }else{
        newDaftarHargaBuah[index] = {name, price, weight}
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${id}`, {name, price, weight})
      }

      setDaftarHargaBuah(newDaftarHargaBuah)
      setInputNamaBuah("")
      setInputHargaBuah(0)
      setInputBeratBuah(0)
      setIndexOfForm(-1)
      setIdOfList(-1)
    }


  }

  return(
    <>
      <TableTitle />
      <table className="margin-center">
        <TableHead />
        <tbody>
            {
              daftarHargaBuah.map((val, index)=>{
                return(
                  <tr key={index}>
                    <td>{val.name}</td>
                    <td>{val.price}</td>
                    <td>{val.weight / 1000} kg</td>
                    <td className="compact">
                      <button id={val.id} onClick={handleEdit} value={index}>Edit</button>
                    </td>
                    <td className="compact">
                      <button id={val.id} onClick={handleDelete} value={index}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>
      {/* Form */}
      <h1>Form Input Buah</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Nama:
          <input
            name="inputNamaBuah"
            type="text"
            value={inputNamaBuah}
            onChange={handleChange}
          />
        </label>
        <label>
          Harga (Rp):
          <input
            name="inputHargaBuah"
            type="number"
            value={inputHargaBuah}
            onChange={handleChange}
          />
        </label>
        <label>
          Berat (g):
          <input
            name="inputBeratBuah"
            type="number"
            value={inputBeratBuah}
            onChange={handleChange}
          />
        </label>
        <button>submit</button>
      </form>
    </>
  )
}

export default FruitLists14
