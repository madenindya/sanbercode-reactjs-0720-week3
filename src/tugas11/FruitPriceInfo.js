import React from 'react';

class TableTitle extends React.Component {
  render() {
    return <h1 class="center">Table Harga Buah</h1>;
  }
}

class TableHead extends React.Component {
    render() {
      return (
          <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
          </tr>
      )
    }
  }

class TableContent extends React.Component {
    render() {
        return (
            <>
            {
                this.props.dataHargaBuah.map(el => {
                    return (
                        <FruitPriceDetail nama={el.nama} harga={el.harga} berat={el.berat} />
                    )
                })
            }
            </>
        )
    }
}

class FruitPriceDetail extends React.Component {
  render() {
    return (
        <tr>
            <td>{this.props.nama}</td>
            <td>{this.props.harga}</td>
            <td>{(this.props.berat / 1000.0)} kg</td>
        </tr>
    )
  }
}

const dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

class FruitPriceInfo extends React.Component {
  render() {
    return (
      <>
        <TableTitle />
        <table class="margin-center">
            <TableHead />
            <TableContent dataHargaBuah={dataHargaBuah} />
        </table>
      </>
    )
  }
}

export default FruitPriceInfo
