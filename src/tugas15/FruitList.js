import React, {useContext} from "react"
import {FruitContext} from "./FruitContext"
import axios from "axios";

class TableTitle extends React.Component {
  render() {
    return <h1 className="center">Table Harga Buah</h1>;
  }
}

class TableHead extends React.Component {
  render() {
    return (
      <thead>
        <tr>
          <th>Nama</th>
          <th>Harga</th>
          <th>Berat</th>
        </tr>
      </thead>
    );
  }
}

const FruitList = () =>{
  const [
    daftarHargaBuah, setDaftarHargaBuah,
    inputNamaBuah, setInputNamaBuah,
    inputHargaBuah, setInputHargaBuah,
    inputBeratBuah, setInputBeratBuah,
    indexOfForm, setIndexOfForm,
    idOfList, setIdOfList
  ] = useContext(FruitContext);

  const handleEdit = (event) =>{
    let index = event.target.value;
    let id = event.target.id;
    let dataBuah = daftarHargaBuah[index];

    setInputNamaBuah(dataBuah.name)
    setInputHargaBuah(dataBuah.price)
    setInputBeratBuah(dataBuah.weight)
    setIndexOfForm(index)
    setIdOfList(id);
  }

  const handleDelete = (event) => {
    let index = event.target.value
    let id = event.target.id;
    let newDaftarHargaBuah = daftarHargaBuah
    newDaftarHargaBuah.splice(index, 1)

    setDaftarHargaBuah([...newDaftarHargaBuah])

    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
  }


  return(
    <>
      <TableTitle />
      <table className="margin-center">
        <TableHead />
        <tbody>
          {
            daftarHargaBuah.map((val, index)=>{
              return(
                <tr key={index}>
                  <td>{val.name}</td>
                  <td>{val.price}</td>
                  <td>{val.weight / 1000} kg</td>
                  <td className="compact">
                    <button id={val.id} onClick={handleEdit} value={index}>Edit</button>
                  </td>
                  <td className="compact">
                    <button id={val.id} onClick={handleDelete} value={index}>Delete</button>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    </>
  )

}

export default FruitList
