import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const FruitContext = createContext();

export const FruitProvider = props => {
  const [daftarHargaBuah, setDaftarHargaBuah] =  useState([])
  const [inputNamaBuah, setInputNamaBuah]  =  useState("")
  const [inputHargaBuah, setInputHargaBuah]  =  useState(0)
  const [inputBeratBuah, setInputBeratBuah]  =  useState(0)
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [idOfList, setIdOfList] =  useState(-1)

  useEffect(() => {
    if (daftarHargaBuah.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setDaftarHargaBuah(newData);
        })
    }
  })

  return (
    <FruitContext.Provider value={
        [
          daftarHargaBuah, setDaftarHargaBuah,
          inputNamaBuah, setInputNamaBuah,
          inputHargaBuah, setInputHargaBuah,
          inputBeratBuah, setInputBeratBuah,
          indexOfForm, setIndexOfForm,
          idOfList, setIdOfList
        ]
    }>
        {props.children}
    </FruitContext.Provider>
  );
};
