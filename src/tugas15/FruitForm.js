import React, {useContext} from "react";
import {FruitContext} from "./FruitContext";
import axios from 'axios';

const FruitForm = () =>{
  const [
    daftarHargaBuah, setDaftarHargaBuah,
    inputNamaBuah, setInputNamaBuah,
    inputHargaBuah, setInputHargaBuah,
    inputBeratBuah, setInputBeratBuah,
    indexOfForm, setIndexOfForm,
    idOfList, setIdOfList
  ] = useContext(FruitContext);


  const handleSubmit = (event) => {
    // menahan submit
    event.preventDefault()

    let name = inputNamaBuah
    let price = +inputHargaBuah
    let weight = +inputBeratBuah


    if (name.replace(/\s/g,'') !== ""){
      let newDaftarHargaBuah = daftarHargaBuah
      let index = indexOfForm;
      let id = idOfList;

      console.log(indexOfForm, id);

      if (index === -1){
        newDaftarHargaBuah = [...newDaftarHargaBuah, {name, price, weight}]
        axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {name, price, weight})
      }else{
        newDaftarHargaBuah[index] = {name, price, weight}
        axios.put(`http://backendexample.sanbercloud.com/api/fruits/${id}`, {name, price, weight})
      }

      setDaftarHargaBuah(newDaftarHargaBuah)
      setInputNamaBuah("")
      setInputHargaBuah(0)
      setInputBeratBuah(0)
      setIndexOfForm(-1)
      setIdOfList(-1)
    }

  }

  const handleChange = (event) =>{
    const targetName = event.target.name;
    const targetValue = event.target.value;

    if (targetName === "inputNamaBuah") {
      setInputNamaBuah(targetValue);
    } else if (targetName === "inputHargaBuah") {
      setInputHargaBuah(targetValue);
    } else if (targetName === "inputBeratBuah") {
      setInputBeratBuah(targetValue);
    }
  }

  return(
    <>
      <h1>Form Input Buah</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Nama:
          <input
            name="inputNamaBuah"
            type="text"
            value={inputNamaBuah}
            onChange={handleChange}
          />
        </label>
        <label>
          Harga (Rp):
          <input
            name="inputHargaBuah"
            type="number"
            value={inputHargaBuah}
            onChange={handleChange}
          />
        </label>
        <label>
          Berat (g):
          <input
            name="inputBeratBuah"
            type="number"
            value={inputBeratBuah}
            onChange={handleChange}
          />
        </label>
        <button>submit</button>
      </form>
    </>
  )

}

export default FruitForm
