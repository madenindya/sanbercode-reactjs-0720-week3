import React from "react";
import { Switch, Route } from "react-router";

import FruitPriceInfo from '../tugas11/FruitPriceInfo';
import Timer from '../tugas12/Timer';
import FruitLists13 from '../tugas13/FruitLists';
import FruitLists14 from '../tugas14/FruitLists';
import Fruit from './Fruit';


const Routes = () => {

  return (
    <div className="App">
      <Switch>
        <Route exact path="/">
          <Fruit />
        </Route>
        <Route path="/tugas11">
          <FruitPriceInfo />
        </Route>
        <Route exact path="/tugas12">
          <Timer />
        </Route>
        <Route exact path="/tugas13">
          <FruitLists13 />
        </Route>
        <Route exact path="/tugas14">
          <FruitLists14 />
        </Route>
        <Route exact path="/tugas15">
          <Fruit />
        </Route>
      </Switch>
    </div>
  );
};

export default Routes;
