import React from "react"
import {ColorProvider} from "./ColorContext"
import NavColorList from "./NavColorList"

const Fruit = () =>{
  return(
    <ColorProvider>
      <NavColorList/>
    </ColorProvider>
  )
}

export default Fruit
