import React, { Component } from "react";

class Timer extends Component {
  constructor(props) {
    super(props);
    const d = new Date();
    let hour = d.getHours();
    const ampm = hour >= 12 ? "PM" : "AM";
    if (hour > 12) {
      hour %= 12;
    } else if (hour === 0) {
      hour = 12;
    }
    this.state = {
      hour,
      minute: d.getMinutes(),
      second: d.getSeconds(),
      ampm,
      counter: 100,
    };
  }

  componentDidMount() {
    if (this.props.start !== undefined) {
      this.setState({
        hour: this.props.start,
        minute: this.props.start,
        second: this.props.start,
        ampm: this.props.start,
        counter: this.props.start,
      });
    }
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    let hour = this.state.hour;
    let minute = this.state.minute;
    let second = this.state.second;
    let ampm = this.state.ampm;
    second++;
    if (second === 60) {
      second = 0;
      minute++;
    }
    if (minute === 60) {
      minute = 0;
      hour++;
    }
    if (hour === 12) {
      ampm = ampm === "AM" ? "PM" : "AM"
    } else if (hour === 13) {
      hour = 1
    }
    this.setState({
      hour,
      minute,
      second,
      ampm,
      counter: this.state.counter - 1,
    });
  }

  render() {
    if (this.state.counter >= 0) {
      return (
        <>
          <div class="margin-center col-80">
            <div class="col-50">
              <h1>
                sekarang jam: {this.state.hour}:{this.state.minute}:{this.state.second} {this.state.ampm}
              </h1>
            </div>
            <div class="col-50">
              <h1>hitung mundur: {this.state.counter}</h1>
            </div>
          </div>
        </>
      );
    } else {
      return <></>;
    }
  }
}

export default Timer;
