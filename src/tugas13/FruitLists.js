import React from "react"

class TableTitle extends React.Component {
  render() {
    return <h1 className="center">Table Harga Buah</h1>;
  }
}

class TableHead extends React.Component {
    render() {
      return (
          <thead>
            <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
            </tr>
          </thead>
      )
    }
  }

class FruitLists13 extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dataHargaBuah : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      inputNamaBuah : "",
      inputHargaBuah : 0,
      inputBeratBuah : 0,
      updateIdx : -1,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate(event) {
    const buttonId = +event.target.id;
    const daftar = this.state.dataHargaBuah;
    const dataBuah = daftar[buttonId]

    console.log(buttonId, dataBuah)

    this.setState({
      inputNamaBuah: dataBuah.nama,
      inputHargaBuah: dataBuah.harga,
      inputBeratBuah: dataBuah.berat,
      updateIdx: buttonId,
    })
  }

  handleDelete(event) {
    const buttonId = +event.target.id;

    const updateHargaBuah = this.state.dataHargaBuah;
    updateHargaBuah[buttonId]['deleted'] = true;

    this.setState(
      {
        dataHargaBuah: updateHargaBuah
      }
    )
  }

  handleChange(event) {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(
      {
        [targetName]: targetValue
      }
    );
  }

  handleSubmit(event) {
    event.preventDefault()
    const updateDaftar = this.state.dataHargaBuah;
    const newBuah = {nama: this.state.inputNamaBuah, harga: +this.state.inputHargaBuah, berat: +this.state.inputBeratBuah};
    const updateId = this.state.updateIdx;
    if (updateId < 0) {
      updateDaftar.push(newBuah)
    } else {
      updateDaftar[updateId] = newBuah;
    }
    this.setState(
      {
        dataHargaBuah: updateDaftar,
        inputNamaBuah: "",
        inputHargaBuah : 0,
        inputBeratBuah : 0,
        updateIdx: -1,
      }
    )
  }

  render() {
    return (
      <>
        <TableTitle />
        <table className="margin-center">
            <TableHead />
            <tbody>
              {
                  this.state.dataHargaBuah.map((el, idx) => {
                    if (el.deleted) {
                      return (<></>)
                    } else {
                      return (
                          <tr key={idx}>
                              <td>{el.nama}</td>
                              <td>{el.harga}</td>
                              <td>{(el.berat / 1000.0)} kg</td>
                              <td className="compact">
                                <button id={idx} onClick={this.handleUpdate}>Edit</button>
                              </td>
                              <td className="compact">
                                <button id={idx} onClick={this.handleDelete}>Delete</button>
                              </td>
                          </tr>
                      )
                    }
                  })
              }
            </tbody>
        </table>

        {/* Form */}
        <h1>Form Input Buah</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Nama:
            <input
              name="inputNamaBuah"
              type="text"
              value={this.state.inputNamaBuah}
              onChange={this.handleChange}/>
          </label>
          <label>
            Harga (Rp):
            <input
              name="inputHargaBuah"
              type="number"
              value={this.state.inputHargaBuah}
              onChange={this.handleChange}/>
          </label>
          <label>
            Berat (g):
            <input
              name="inputBeratBuah"
              type="number"
              value={this.state.inputBeratBuah}
              onChange={this.handleChange} />
          </label>
          <button>submit</button>
        </form>
      </>
    )
  }
}

export default FruitLists13
